jQuery(document).ready(function($) {
  var settings = Drupal.settings;

  var fields = settings.geolocationGooglemaps.formatters;
  $.each(fields, function(instance, data) {
    $.each(data.deltas, function(d, delta) {
      var geocoder = new google.maps.Geocoder();
      var lat = delta.lat;
      var long = delta.lng;
      var latlng = new google.maps.LatLng(lat, long);

      geocoder.geocode({ 'latLng': latlng }, function(data, status) {
        if(status == google.maps.GeocoderStatus.OK) {
          $("[id*='" + instance + "-" + d + "']").parent().before('<div>' + data[0].formatted_address + '</div>');
        }
      });
    });
  });
});

